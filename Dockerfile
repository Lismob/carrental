FROM openjdk:11
EXPOSE 8080
COPY target/car_rental-0.0.1-SNAPSHOT.jar app.jar
ENV POSTGRES_SCHEMA "car_rental"
ENTRYPOINT ["java","-XX:MaxRAM=100M", "-jar", "/app.jar"]