CREATE SCHEMA IF NOT EXISTS car_rental;
create table if not exists car_rental.car
(
    id                 bigserial
    primary key,
    brand              varchar(255),
    color              varchar(255),
    model              varchar(255),
    number             varchar(255),
    price              integer not null
    );

create table if not exists car_rental.client
(
    id           bigserial
    primary key,
    name         varchar(255),
    phone_number varchar(255) not null,
    telegram_name varchar(255) not null,
    surname      varchar(255)
    );

create table if not exists car_rental.users
(
    id        bigserial
    primary key,
    login     varchar(255),
    password  varchar(255),
    role      integer,
    client_id bigint
    constraint users_client
    references car_rental.client
    );

create table if not exists car_rental.orders
(
    id              bigserial
    primary key,
    car_return_time timestamp,
    created_on      timestamp,
    last_updated_on timestamp,
    receipt_time    timestamp,
    status          integer,
    car_id          bigint
    constraint orders_car
    references car_rental.car,
    client_id       bigint
    constraint orders_client
    references car_rental.client
);



