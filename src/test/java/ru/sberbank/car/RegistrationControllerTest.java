package ru.sberbank.car;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.Rollback;
import ru.sberbank.car.model.Client;
import ru.sberbank.car.model.Orders;
import ru.sberbank.car.model.User;
import ru.sberbank.car.model.enam.Role;
import ru.sberbank.car.repository.UserRepository;

import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WithMockUser(authorities = {"admin"})
class RegistrationControllerTest extends CarRentalApplicationTests {
    @Autowired
    UserRepository userRepository;

    @Test
    void testGetLogin() throws Exception {
        mockMvc.perform(get("/login"))
                .andExpect(status().isOk())
                .andExpect(view().name("security/login"));
    }

    @Test
    void testRegistration() throws Exception {
        mockMvc.perform(get("/registration"))
                .andExpect(status().isOk())
                .andExpect(view().name("security/registration"));
    }

    @Test
    void testAddUserError() throws Exception {
        Map<String, Object> stringListMap = new HashMap<>();
        stringListMap.put("userForm", new User());

        mockMvc.perform(post("/registration").flashAttrs(stringListMap).with(csrf()))
                .andExpect(status().isOk())
                .andExpect(view().name("security/registration"))
                .andExpect(model().attributeErrorCount("userForm", 2));
    }

    @Test
    @Transactional
    @Rollback
    void testAddUser() throws Exception {
        User user = new User(4L, "test", "testPassword", Role.CLIENT,
                new Client(4L, "testName", "testSurName", "telegramName", "89999999999", new HashSet<Orders>()));
        Map<String, Object> stringListMap = Map.of("userForm", user);

        mockMvc.perform(post("/registration").flashAttrs(stringListMap).with(csrf()))
                .andExpect(status().is3xxRedirection())
                .andExpect(header().string("Location", "/car"));

        assertEquals(4, userRepository.findAll().size());
    }
}