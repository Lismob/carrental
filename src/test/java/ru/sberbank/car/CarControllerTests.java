package ru.sberbank.car;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.Rollback;
import ru.sberbank.car.model.Car;
import ru.sberbank.car.repository.CarRepository;

import javax.transaction.Transactional;
import java.util.Map;

import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WithMockUser(authorities = {"admin"})
public class CarControllerTests extends CarRentalApplicationTests {

    @Autowired
    CarRepository carRepository;

    @Test
    void testListCar() throws Exception {
        mockMvc.perform(get("/car"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("MAZDA")))
                .andExpect(content().string(containsString("LADA")))
                .andExpect(view().name("car/listCar"));
    }

    @Test
    void testShowCar() throws Exception {
        mockMvc.perform(get("/car/1"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("BMW")))
                .andExpect(view().name("car/showCar"));
    }

    @Test
    void testShowNewCarPage() throws Exception {
        mockMvc.perform(get("/car/new"))
                .andExpect(status().isOk())
                .andExpect(view().name("car/newCar"));
    }

    @Test
    void testAddCarWithError() throws Exception {
        Map<String, Object> stringListMap = new java.util.HashMap<>();
        stringListMap.put("newCar", new Car());

        mockMvc.perform(post("/car/add").flashAttrs(stringListMap).with(csrf()))
                .andExpect(status().isOk())
                .andExpect(view().name("car/newCar"))
                .andExpect(model().attributeErrorCount("newCar", 4));
    }

    @Test
    @Transactional
    @Rollback
    void testAddCar() throws Exception {
        Car car = new Car(4L, "KIA", "RIO", "WHITE", "851", 5000, null);
        Map<String, Object> stringListMap = Map.of("newCar", car);

        mockMvc.perform(post("/car/add").flashAttrs(stringListMap).with(csrf()))
                .andExpect(status().is3xxRedirection())
                .andExpect(header().string("Location", "/car"));

        assertEquals(4, carRepository.findAll().size());
    }

    @Test
    void testShowUpdateCarPage() throws Exception {
        mockMvc.perform(get("/car/update/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("car/updateCar"));
    }

    @Test
    @Transactional
    @Rollback
    void testUpdateCar() throws Exception {
        Car car = new Car(3L, "KIA", "RIO", "WHITE", "851", 5000, null);
        Map<String, Object> stringListMap = Map.of("updateCar", car);


        mockMvc.perform(patch("/car/update").flashAttrs(stringListMap).with(csrf()))
                .andExpect(status().is3xxRedirection())
                .andExpect(header().string("Location", "/car"));

        assertEquals(car, carRepository.findById(3L).get());

    }

    @Test
    @Transactional
    @Rollback
    void testDeleteCar() throws Exception {

        mockMvc.perform(delete("/car/delete/3").with(csrf()))
                .andExpect(status().is3xxRedirection())
                .andExpect(header().string("Location", "/car"));

        assertEquals(2, carRepository.findAll().size());

    }



}
