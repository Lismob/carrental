package ru.sberbank.car;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.Rollback;
import ru.sberbank.car.model.Car;
import ru.sberbank.car.model.Client;
import ru.sberbank.car.model.Orders;
import ru.sberbank.car.model.User;
import ru.sberbank.car.model.enam.Role;
import ru.sberbank.car.repository.CarRepository;
import ru.sberbank.car.repository.UserRepository;

import javax.transaction.Transactional;
import java.util.*;

import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WithMockUser(authorities = {"admin"})
class UserControllerTest extends CarRentalApplicationTests {
    @Autowired
    UserRepository userRepository;

    @Test
    void testGetUserList() throws Exception {
        //отредактировать данные
        mockMvc.perform(get("/user"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("kirill")))
                .andExpect(content().string(containsString("masha")))
                .andExpect(content().string(containsString("sasha")))
                .andExpect(view().name("user/listUser"));
    }

    @Test
    void show() throws Exception {
        mockMvc.perform(get("/user/2"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("kirill")))
                .andExpect(view().name("user/showUser"));
    }

    @Test
    void testViewAddUserGet() throws Exception {
        mockMvc.perform(get("/user/new"))
                .andExpect(status().isOk())
                .andExpect(view().name("user/newUser"));
    }

    @Test
    void testAddUserWithError() throws Exception {
        Map<String, Object> stringListMap = new java.util.HashMap<>();
        stringListMap.put("newUser", new User());

        mockMvc.perform(post("/user/add").flashAttrs(stringListMap).with(csrf()))
                .andExpect(status().isOk())
                .andExpect(view().name("user/newUser"))
                .andExpect(model().attributeErrorCount("newUser", 2));
    }
    @Test
    @Transactional
    @Rollback
    void testViewAddUserPost() throws Exception {
        User user = new User(4L,"test", "testPassword", Role.CLIENT,null);
        Map<String, Object> stringListMap = Map.of("newUser", user);

        mockMvc.perform(post("/user/add").flashAttrs(stringListMap).with(csrf()))
                .andExpect(status().isOk())
                .andExpect(view().name("user/newClient"));

        assertEquals(4, userRepository.findAll().size());
    }
    @Test
    void testUpdateUserGet() throws Exception {
        mockMvc.perform(get("/user/update/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("user/updateUser"));
    }

    @Test
    @Transactional
    @Rollback
    void testViewUpdateUser() throws Exception {
        User user = new User(4L,"igor", "12345", Role.ADMIN,
               null);
        Map<String, Object> stringListMap = Map.of("updateUser", user);

        mockMvc.perform(patch("/user/update").flashAttrs(stringListMap).with(csrf()))
                .andExpect(status().is3xxRedirection())
                .andExpect(header().string("Location", "/user"));

        assertEquals(user, userRepository.findById(4L).get());
    }

    @Test
    @Transactional
    @Rollback
    void testDeleteUser() throws Exception {

        mockMvc.perform(delete("/user/delete/1").with(csrf()))
                .andExpect(status().is3xxRedirection())
                .andExpect(header().string("Location", "/user"));

        assertEquals(3, userRepository.findAll().size());
    }
}