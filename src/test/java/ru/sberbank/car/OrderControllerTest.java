package ru.sberbank.car;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.Rollback;
import org.springframework.web.bind.annotation.PostMapping;
import ru.sberbank.car.model.Car;
import ru.sberbank.car.model.Client;
import ru.sberbank.car.model.Orders;
import ru.sberbank.car.model.enam.OrderStatus;
import ru.sberbank.car.repository.CarRepository;
import ru.sberbank.car.repository.OrdersRepository;

import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WithMockUser(authorities = {"admin"})
class OrderControllerTest extends CarRentalApplicationTests {
    @Autowired
    private OrdersRepository ordersRepository;
// отредактировать данныйе

    @Test
    void testGetListOrder() throws Exception {
        mockMvc.perform(get("/order"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Kirill")))
                .andExpect(view().name("orders/listOrder"));
    }

    @Test
    void testShowOrder() throws Exception {
        mockMvc.perform(get("/order/1"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Kirill")))
                .andExpect(view().name("orders/showOrder"));
    }

    @Test
    @WithMockUser(authorities = {"client"})
    @Transactional
    @Rollback
    public void testAddOrder() throws Exception {
        Orders orders = new Orders(2L, new Car(3L, "KIA", "RIO", "WHITE", "851", 5000, null),
                new Client(2L, "testName", "testSurName", "telegramName", "89999999999", new HashSet<Orders>()),
                OrderStatus.PENDING, null, null, null, null);
        Map<String, Object> stringListMap = Map.of("order", orders);

        mockMvc.perform(post("/order/addOrder").flashAttrs(stringListMap).with(csrf()))
                .andExpect(status().is3xxRedirection())
                .andExpect(header().string("Location", "/order/myOrders"));

        assertEquals(2, ordersRepository.findAll().size());
    }

    @Test
    void testShowUpdateOrder() throws Exception {
        mockMvc.perform(get("/order/update/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("orders/updateOrder"));
    }

    @Test
    @Transactional
    @Rollback
    void testUpdateOrder() throws Exception {
        Orders orders = new Orders(1L, new Car(3L, "MAZDA", "x6", "BLUE", "321", 1000, null),
                new Client(2L, "Masha", "Mashina", "telegramNameMasha", "85558887777", new HashSet<Orders>()),
                OrderStatus.PENDING, null, null, null, null);
        Map<String, Object> stringListMap = Map.of("updateOrder", orders);

        mockMvc.perform(patch("/order/update").flashAttrs(stringListMap).with(csrf()))
                .andExpect(status().is3xxRedirection())
                .andExpect(header().string("Location", "/order"));

        assertEquals(orders, ordersRepository.findById(1L).get());

    }

    @Test
    @Transactional
    @Rollback
    void testDeleteOrder() throws Exception {

        mockMvc.perform(delete("/order/delete/1").with(csrf()))
                .andExpect(status().is3xxRedirection())
                .andExpect(header().string("Location", "/order"));

        assertEquals(0, ordersRepository.findAll().size());

    }
}

