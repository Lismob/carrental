package ru.sberbank.car.utils;

public class Consts {

    public static final String START_MESSAGE = "Приветствую! Это CarRental - бот, который поможет вам найти ваши заказы";

    public static final String UNKNOWN_COMMAND = "Извините, я не знаю такой команды";
    public static final String CANT_UNDERSTAND = "Извините, я не понял, что вы имеете ввиду";


}
