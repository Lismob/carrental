package ru.sberbank.car.service;

import ru.sberbank.car.model.Car;

import java.util.List;

public interface CarService {
    List<Car> getAllCar();

    List<Car> getAllFreeCar();

    Car getCar(long id);

    void saveCar(Car car);

    void updateCar(Car car);

    void deleteCar(long id);
}
