package ru.sberbank.car.service;

import ru.sberbank.car.model.Client;

import java.util.List;

public interface ClientService {
    List<Client> getAllClient();

    Client getClient(long id);

    void saveClient(Client client);

    void getClientByTelegramName(String telegramName);

    void deleteClient(long id);

}
