package ru.sberbank.car.service;

import ru.sberbank.car.model.Client;
import ru.sberbank.car.model.Orders;

import java.util.List;

public interface OrdersService {
    List<Orders> getAllOrders();

    Orders getOrders(long id);

    void saveOrders(Orders orders);

    void updateOrder(Orders orders);

    List<Orders> findOrdersByClient(Client client);

    List<Orders> findOrdersByTelegramName(String telegramName);

    void deleteOrders(long id);
}
