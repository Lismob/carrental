package ru.sberbank.car.service;

import org.springframework.security.core.userdetails.UserDetailsService;
import ru.sberbank.car.model.User;

import java.util.List;

public interface UserService extends UserDetailsService {
    List<User> getAllUsers();

    User getUser(long id);

    void saveUser(User users);

    void updateUser(User user);

    void deleteUser(long id);
}
