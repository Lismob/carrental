package ru.sberbank.car.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sberbank.car.model.Car;
import ru.sberbank.car.repository.CarRepository;
import ru.sberbank.car.service.CarService;

import java.util.List;
import java.util.Optional;

@Service
public class CarServiceImp implements CarService {
    @Autowired
    private CarRepository carRepository;

    @Override
    public List<Car> getAllCar() {
        return carRepository.findAll();
    }

    @Override
    public List<Car> getAllFreeCar() {
        return carRepository.findCarsByOrdersIsNull();
    }

    @Override
    public Car getCar(long id) {
        Car car = null;
        Optional<Car> optional = carRepository.findById(id);
        if (optional.isPresent()) {
            car = optional.get();
        }
        return car;
    }

    @Override
    public void saveCar(Car car) {
        carRepository.save(car);
    }

    @Override
    public void updateCar(Car car) {
        carRepository.save(car);
    }

    @Override
    public void deleteCar(long id) {
        carRepository.deleteById(id);
    }
}
