package ru.sberbank.car.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sberbank.car.model.Client;
import ru.sberbank.car.repository.ClientRepository;
import ru.sberbank.car.service.ClientService;

import java.util.List;
import java.util.Optional;

@Service
public class ClientServiceImp implements ClientService {
    @Autowired
    private ClientRepository clientRepository;

    @Override
    public List<Client> getAllClient() {
        return clientRepository.findAll();
    }

    @Override
    public Client getClient(long id) {
        Client client = null;
        Optional<Client> optional = clientRepository.findById(id);
        if (optional.isPresent()) {
            client = optional.get();
        }
        return client;
    }

    @Override
    public void saveClient(Client client) {
        clientRepository.save(client);
    }

    @Override
    public void getClientByTelegramName(String telegramName) {
        clientRepository.getClientByTelegramName(telegramName);
    }

    @Override
    public void deleteClient(long id) {
        clientRepository.deleteById(id);
    }
}
