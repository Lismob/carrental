package ru.sberbank.car.service.Impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.sberbank.car.model.Client;
import ru.sberbank.car.model.Orders;
import ru.sberbank.car.repository.ClientRepository;
import ru.sberbank.car.repository.OrdersRepository;
import ru.sberbank.car.service.OrdersService;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class OrdersServiceImp implements OrdersService {
    private final OrdersRepository ordersRepository;

    private final ClientRepository clientRepository;

    @Override
    public List<Orders> getAllOrders() {
        return ordersRepository.findAll();
    }

    @Override
    public Orders getOrders(long id) {
        Orders orders = null;
        Optional<Orders> optional = ordersRepository.findById(id);
        if (optional.isPresent()) {
            orders = optional.get();
        }
        return orders;
    }

    @Override
    public void saveOrders(Orders orders) {
        ordersRepository.save(orders);
    }

    @Override
    public void updateOrder(Orders orders) {
        ordersRepository.save(orders);
    }

    @Override
    public List<Orders> findOrdersByTelegramName(String telegramName) {
        Client client = clientRepository.getClientByTelegramName(telegramName);
        return ordersRepository.findOrdersByClient(client);
    }

    @Override
    public List<Orders> findOrdersByClient(Client client) {
        return ordersRepository.findOrdersByClient(client);
    }


    @Override
    public void deleteOrders(long id) {
        ordersRepository.deleteById(id);
    }
}
