package ru.sberbank.car.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.sberbank.car.model.Client;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {
    Client getClientByTelegramName(String telegramName);
}
