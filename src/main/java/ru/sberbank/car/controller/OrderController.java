package ru.sberbank.car.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.sberbank.car.model.Car;
import ru.sberbank.car.model.Client;
import ru.sberbank.car.model.Orders;
import ru.sberbank.car.model.User;
import ru.sberbank.car.model.enam.OrderStatus;
import ru.sberbank.car.service.CarService;
import ru.sberbank.car.service.OrdersService;

import javax.validation.Valid;


@Controller
@RequestMapping("/order")
public class OrderController {
    private final OrdersService ordersService;
    private final CarService carService;

    public OrderController(OrdersService ordersService, CarService carService) {
        this.ordersService = ordersService;
        this.carService = carService;
    }

    @GetMapping()
    @PreAuthorize("hasAnyAuthority('admin', 'manager')")
    public String index(Model model) {
        model.addAttribute("orders", ordersService.getAllOrders());
        return "orders/listOrder";
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('admin', 'manager')")
    public String show(@PathVariable("id") long id, Model model) {
        model.addAttribute("order", ordersService.getOrders(id));
        return "orders/showOrder";
    }

    @GetMapping("/new")
    @PreAuthorize("hasAnyAuthority('client')")
    public String creatOrder(@ModelAttribute("car") Car oldCar, Model model) {
        Client client = ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getClient();
        Car car = carService.getCar(oldCar.getId());
        model.addAttribute("order", new Orders(car, client));
        return "orders/newOrder";
    }

    @PostMapping("/addOrder")
    @PreAuthorize("hasAnyAuthority('client')")
    public String creatOrder(@ModelAttribute("order") @Valid Orders order, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "orders/newOrder";
        }
        ordersService.saveOrders(order);
        return "redirect:/order/myOrders";
    }

    @GetMapping("/myOrders")
    @PreAuthorize("hasAnyAuthority('client')")
    public String showOrderClient(Model model) {
        Client client = ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getClient();
        model.addAttribute("orderList", ordersService.findOrdersByClient(client));
        return "orders/showOrdersClient";
    }

    @GetMapping("/update/{id}")
    @PreAuthorize("hasAnyAuthority('admin', 'manager')")
    public String viewUpdateCar(@PathVariable("id") long id, Model model) {
        model.addAttribute("updateOrder", ordersService.getOrders(id));
        model.addAttribute("orderStatus", OrderStatus.values());
        return "orders/updateOrder";
    }

    @PatchMapping("/update")
    @PreAuthorize("hasAnyAuthority('admin', 'manager')")
    public String updateCar(@ModelAttribute("updateOrder") @Valid Orders updateOrder, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "orders/updateOrder";
        }
        ordersService.updateOrder(updateOrder);
        return "redirect:/order";
    }

    @DeleteMapping("/delete/{id}")
    @PreAuthorize("hasAnyAuthority('admin', 'manager')")
    public String deleteCar(@PathVariable("id") long id) {
        ordersService.deleteOrders(id);
        return "redirect:/order";
    }

}
