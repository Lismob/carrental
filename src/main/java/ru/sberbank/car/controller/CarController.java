package ru.sberbank.car.controller;


import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.sberbank.car.model.Car;
import ru.sberbank.car.service.CarService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequestMapping("/car")
public class CarController {
    private final CarService carService;

    public CarController(CarService carService) {
        this.carService = carService;
    }

    @GetMapping()
    public String index(Model model) {
        model.addAttribute("cars", carService.getAllFreeCar());
        return "car/listCar";
    }

    @GetMapping("/{id}")
    public String show(@PathVariable("id") long id, Model model) {
        model.addAttribute("car", carService.getCar(id));
        return "car/showCar";
    }

    @GetMapping("/new")
    @PreAuthorize("hasAnyAuthority('admin', 'manager')")
    public String viewAddCar(Model model) {
        model.addAttribute("newCar", new Car());
        return "car/newCar";
    }

    @PostMapping("/add")
    @PreAuthorize("hasAnyAuthority('admin', 'manager')")
    public String addCar(@ModelAttribute("newCar") @Valid Car newCar, BindingResult bindingResult, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            return "car/newCar";
        }
        carService.saveCar(newCar);
        return "redirect:/car";
    }

    @GetMapping("/update/{id}")
    @PreAuthorize("hasAnyAuthority('admin', 'manager')")
    public String viewUpdateCar(@PathVariable("id") long id, Model model) {
        model.addAttribute("updateCar", carService.getCar(id));
        return "car/updateCar";
    }

    @PatchMapping("/update")
    @PreAuthorize("hasAnyAuthority('admin', 'manager')")
    public String updateCar(@ModelAttribute("updateCar") @Valid Car newCar, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "car/updateCar";
        }
        carService.updateCar(newCar);
        return "redirect:/car";
    }

    @DeleteMapping("/delete/{id}")
    @PreAuthorize("hasAnyAuthority('admin', 'manager')")
    public String deleteCar(@PathVariable("id") long id) {
        carService.deleteCar(id);
        return "redirect:/car";
    }

}
