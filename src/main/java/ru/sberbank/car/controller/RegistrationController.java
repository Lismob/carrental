package ru.sberbank.car.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.sberbank.car.model.User;
import ru.sberbank.car.model.enam.Role;
import ru.sberbank.car.service.UserService;

import javax.validation.Valid;

@Controller("/")
public class RegistrationController {

    @Autowired
    private UserService userService;

    @GetMapping("/login")
    public String getLogin() {
        return "security/login";
    }

    @GetMapping("/registration")
    public String registration(Model model) {
        model.addAttribute("userForm", new User());
        return "security/registration";
    }

    @PostMapping("/registration")
    public String addUser(@Valid @ModelAttribute("userForm") User userForm, BindingResult bindingResult, SecurityContextHolder securityContextHolder) {

        if (bindingResult.hasErrors()) {
            return "security/registration";
        }
        userForm.setRole(Role.CLIENT);
        userService.saveUser(userForm);

        return "redirect:/car";
    }
}