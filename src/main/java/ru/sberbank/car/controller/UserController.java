package ru.sberbank.car.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.sberbank.car.model.Client;
import ru.sberbank.car.model.User;
import ru.sberbank.car.model.enam.Role;
import ru.sberbank.car.service.UserService;

import javax.validation.Valid;
import java.util.Objects;


@Controller
@RequestMapping("/user")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping()
    public String index(Model model) {
        model.addAttribute("users", userService.getAllUsers());
        return "user/listUser";
    }

    @GetMapping("/{id}")
    public String show(@PathVariable("id") long id, Model model) {
        model.addAttribute("user", userService.getUser(id));
        return "user/showUser";
    }

    @GetMapping("/new")
    public String viewAddUser(Model model) {
        model.addAttribute("newUser", new User());
        model.addAttribute("roles", Role.values());
        return "user/newUser";
    }

    @PostMapping("/add")
    public String addUser(Model model, @ModelAttribute("newUser") @Valid User newUser, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("roles", Role.values());
            return "user/newUser";
        }
        if (newUser.getRole().equals(Role.CLIENT) && Objects.isNull(newUser.getClient())) {
            newUser.setClient(new Client());
            return "user/newClient";
        }
        userService.saveUser(newUser);
        return "redirect:/user";
    }

    @GetMapping("/update/{id}")
    public String viewUpdateUser(@PathVariable("id") long id, Model model) {
        model.addAttribute("updateUser", userService.getUser(id));
        model.addAttribute("roles", Role.values());
        return "user/updateUser";
    }

    @PatchMapping("/update")
    public String updateUser(Model model, @ModelAttribute("updateUser") @Valid User newUser, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("roles", Role.values());
            return "user/updateUser";
        }
        userService.updateUser(newUser);
        return "redirect:/user";
    }

    @DeleteMapping("/delete/{id}")
    public String deleteUser(@PathVariable("id") long id) {
        userService.deleteUser(id);
        return "redirect:/user";
    }


}
