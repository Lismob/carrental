package ru.sberbank.car.model;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import ru.sberbank.car.model.enam.OrderStatus;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter
@Setter
public class Orders {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @ManyToOne
    @JoinColumn(name = "car_id")
    @NotNull(message = "Поле машина не может быть пустым")
    Car car;

    @ManyToOne
    @JoinColumn(name = "client_id")
    @NotNull(message = "Поле клиент не может быть пустым")
    Client client;

    private OrderStatus status;

    private LocalDateTime receiptTime;

    private LocalDateTime carReturnTime;


    @CreationTimestamp
    @ToString.Exclude
    private Instant createdOn;

    @UpdateTimestamp
    @ToString.Exclude
    private Instant lastUpdatedOn;


    public Orders(Car car, Client client) {
        this.car = car;
        this.client = client;
        status = OrderStatus.PENDING;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Orders orders = (Orders) o;
        return Objects.equals(id, orders.id) && Objects.equals(car, orders.car) && Objects.equals(client, orders.client) && status == orders.status && Objects.equals(receiptTime, orders.receiptTime) && Objects.equals(carReturnTime, orders.carReturnTime) && Objects.equals(createdOn, orders.createdOn) && Objects.equals(lastUpdatedOn, orders.lastUpdatedOn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, car, client, status, receiptTime, carReturnTime, createdOn, lastUpdatedOn);
    }
}
