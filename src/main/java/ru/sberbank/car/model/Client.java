package ru.sberbank.car.model;

import lombok.*;
import ru.sberbank.car.validate.annotation.Unique;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Objects;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Entity
@Getter
@Setter
@ToString
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotBlank(message = "Поле name не должно быть пустым")
    private String name;
    @NotBlank(message = "Поле surname не должно быть пустым")
    private String surname;
    @Unique
    @NotBlank(message = "Поле telegramName не должно быть пустым")
    private String telegramName;
    @NotBlank(message = "Поле phoneNumber не должно быть пустым")
    private String phoneNumber;

    @OneToMany(mappedBy = "client")
    @ToString.Exclude
    Set<Orders> registrations;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return Objects.equals(id, client.id) && Objects.equals(name, client.name) && Objects.equals(surname, client.surname) && Objects.equals(telegramName, client.telegramName) && Objects.equals(phoneNumber, client.phoneNumber) && Objects.equals(registrations, client.registrations);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, surname, telegramName, phoneNumber, registrations);
    }
}
