package ru.sberbank.car.model.enam;

public enum OrderStatus {
    PENDING("Ожидает подтверждение от менджера"),
    AWAITING_PICKUP("Ожидает получение машины клиентом"),
    PROCESSING("Выполняется"),
    COMPLETED("Завершен");

    private final String description;

    OrderStatus(String description) {
        this.description = description;
    }
}
