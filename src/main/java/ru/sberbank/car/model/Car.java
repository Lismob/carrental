package ru.sberbank.car.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Objects;
import java.util.Set;

@NoArgsConstructor
@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotBlank(message = "Поле model не должно быть пустым")
    @Size(min = 2, message = "Минимальный размер имени 2 буквы")
    private String brand;
    @NotBlank(message = "Поле model не должно быть пустым")
    private String model;
    @NotBlank(message = "Поле color не должно быть пустым")
    private String color;
    @NotBlank(message = "Поле number не должно быть пустым")
    private String number;
    private int price;

    @OneToMany(mappedBy = "car")
    @ToString.Exclude
    Set<Orders> orders;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return price == car.price && Objects.equals(id, car.id) && Objects.equals(brand, car.brand) && Objects.equals(model, car.model) && Objects.equals(color, car.color) && Objects.equals(number, car.number);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, brand, model, color, number, price);
    }
}
