package ru.sberbank.car.validate;

import org.springframework.beans.factory.annotation.Autowired;
import ru.sberbank.car.repository.UserRepository;
import ru.sberbank.car.validate.annotation.Unique;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

public class UniqueValidator implements ConstraintValidator<Unique, String> {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void initialize(Unique contactNumber) {
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return !Objects.nonNull(userRepository) || userRepository.findUserByLogin(value) == null;
    }
}