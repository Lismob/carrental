package ru.sberbank.car.telegram.comand;


import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.sberbank.car.service.OrdersService;

@RequiredArgsConstructor
@Component
public class OrderCommand implements Command {

    private final OrdersService ordersService;

    public SendMessage apply(Update update) {
        long chatId = update.getMessage().getChatId();
        String login = update.getMessage().getFrom().getUserName();
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(String.valueOf(chatId));
        String orders = ordersService.findOrdersByTelegramName(login).stream()
                .map(Object::toString).reduce("Вот ваши заказы: \n", (a, b) -> a.concat(b) + "\n\n");
        sendMessage.setText(orders);

        return sendMessage;
    }

}
