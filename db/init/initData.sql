INSERT INTO car_rental.client (name, surname, phone_number, telegram_name) VALUES ( 'Kirill', 'Romanov', 89155912725, 'lis_mob');
INSERT INTO car_rental.client (name, surname, phone_number, telegram_name) VALUES ( 'Masha', 'Mashina', 85558887777, 'telegramNameMasha');
INSERT INTO car_rental.client (name, surname, phone_number, telegram_name) VALUES ( 'Sasha', 'Sashina', 89998887777, 'telegramNameSasha');

INSERT INTO car_rental.users (login, password, role, client_id) VALUES ( 'kirill', '$2a$10$o1NK30xxtq/vyzNrmpsQYu/4C0O94HblkC3ZMCuxe7OXtaZMsGrVq', 2, 1);
INSERT INTO car_rental.users (login, password, role, client_id) VALUES ( 'oleg', '$2a$10$o1NK30xxtq/vyzNrmpsQYu/4C0O94HblkC3ZMCuxe7OXtaZMsGrVq', 1, null);
INSERT INTO car_rental.users (login, password, role, client_id) VALUES ( 'admin', '$2a$10$o1NK30xxtq/vyzNrmpsQYu/4C0O94HblkC3ZMCuxe7OXtaZMsGrVq', 0, null);

INSERT INTO car_rental.car (brand, color, model, number, price) VALUES ('BMW', 'RED', 'x5', '753', 5000);
INSERT INTO car_rental.car (brand, color, model, number, price) VALUES ('LADA', 'BLUE', '2010', '531', 1000);
INSERT INTO car_rental.car (brand, color, model, number, price) VALUES ('BMW', 'BLUE', 'x6', '123', 1000);
INSERT INTO car_rental.car (brand, color, model, number, price) VALUES ('MAZDA', 'GREEN', '3', '421', 3000);

INSERT INTO car_rental.orders ( car_return_time, created_on, last_updated_on, receipt_time, status, car_id, client_id) VALUES ('2024-03-23 15:35:34.000000', '2024-03-25 15:35:41.000000', '2024-03-22 15:35:45.000000', '2024-03-22 15:35:50.000000', 0, 1, 1);
